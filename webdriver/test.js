const {Builder, By, Key, until} = require('selenium-webdriver');

let driver = new Builder()
    .forBrowser('chrome')
    .build();

driver.get('http://hollysieck.info/Forms/t9practiceform.html')
    .then(_ =>
	driver.findElement(By.name('FirstName')).sendKeys('Holly', Key.RETURN))
		.then (_ =>
		driver.findElement(By.name('LastName')).sendKeys('Sieck', Key.TAB))
			.then (_ =>
			driver.findElement(By.name('password')).sendKeys('1234abcd', Key.TAB))
				.then(_ =>
				driver.findElement(By.name('email')).sendKeys('example@email.com' , Key.RETURN))
					.then(_ => 
					driver.wait(until.titleIs('WDV101 Form Emailer Example'), 10000))
//.then(_ => driver.quit());