const assert = require('assert');
const {Builder, By, Key, until} = require('selenium-webdriver');
let driver = new Builder()
  .forBrowser('chrome')
  .build();
var actual_error = "Value must be greater than or equal to 1.";
var actual_errorC = "Successful submission, quantity is valid.";
var actual_errorE = "Value must be less than or equal to 100.";
var expected_errorA; //-5
var expected_errorB; //0
var expected_errorC; //56
var expected_errorD; //100 
var expected_errorE; //106


async function Assertions() {
 // -5 test
 await driver.get('http://hollysieck.info/wdv495/webdriver/testForm.html');
		
 await driver.findElement(By.id('quantity')).sendKeys('-5');
 await driver.findElement(By.id('submit')).click();
 await driver.findElement(By.id('result'));
 expected_errorA = await driver.findElement(By.id('result')).getText();

 assert.equal(actual_error,expected_errorA, "Error, Test for '-5' failed");
	if (actual_error == expected_errorA) {
		console.log("Test for '-5' was a success!")
	}
	
	else {
		console.log("Error, Test for '-5' failed..")
	}
	
 // 0 test
 await driver.navigate().refresh();
 await driver.findElement(By.id('quantity')).sendKeys('0');
 await driver.findElement(By.id('submit')).click();
 await driver.findElement(By.id('result'));
 expected_errorB = await driver.findElement(By.id('result')).getText();

 assert.equal(actual_error,expected_errorB, "Error, Test for '0' failed");
	if (actual_error == expected_errorB) {
		console.log("Test for '0' was a success!")
	}
	
	else {
		console.log("Error, Test for '0' failed..")
	}
 
	
 //56 Test
 await driver.navigate().refresh();
 await driver.findElement(By.id('quantity')).sendKeys('56');
 await driver.findElement(By.id('submit')).click();
 await driver.findElement(By.id('result'));
 expected_errorC = await driver.findElement(By.id('result')).getText();

 assert.equal(actual_errorC,expected_errorC, "Error, Test for '56' failed");
	if (actual_errorC == expected_errorC) {
		console.log("Test for '56' was a success!")
	}
	
	else {
		console.log("Error, Test for '56' failed..")
	}
	
//100 Test
 await driver.navigate().refresh();
 await driver.findElement(By.id('quantity')).sendKeys('100');
 await driver.findElement(By.id('submit')).click();
 await driver.findElement(By.id('result'));
 expected_errorD = await driver.findElement(By.id('result')).getText();

 assert.equal(actual_errorC,expected_errorD, "Error, Test for '100' failed");
	if (actual_errorC == expected_errorD) {
		console.log("Test for '100' was a success!")
	}
	
	else {
		console.log("Error, Test for '100' failed..")
	}
	
//106 Test
 await driver.navigate().refresh();
 await driver.findElement(By.id('quantity')).sendKeys('106');
 await driver.findElement(By.id('submit')).click();
 await driver.findElement(By.id('result'));
 
 expected_errorE = await driver.findElement(By.id('result')).getText();
 assert.equal(actual_errorE, expected_errorE, "Error, Test for '106' failed"); 

	if (expected_errorE = actual_errorE) {
		console.log("Test for '106' was a success!")
	}
	 
	else {
		console.log("Error, Test for '106' failed!")
	}
}



Assertions();
