<?php
$quanErrMsg = "";

$validForm = false;

$inQuantity = "";

function validateQuantity()
{
	global $inQuantity, $validForm, $quanErrMsg;

	$quanErrMsg = "";

	if($inQuantity == "")
	{
		$validForm = false;
		$quanErrMsg .= "Quantity is required.  ";
	}


	if( intVal($inQuantity)==0 )
	{
		$validForm = false;
		$quanErrMsg .= "Quantity must be an integer.  ";
	}
	else
	{
		if($inQuantity <1 || $inQuantity > 100)
		{
			$validForm = false;
			$quanErrMsg .= "Quantity should be between 1 and 100.";
		}
	}


}

if( isset($_POST['submit']) )
{

	$inQuantity = $_POST['inQuantity'];

	$validForm = true;
		validateQuantity();
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV495 Quantity Form</title>
<style>

#orderArea	{
	width:700px;
}

.error	{
	color:red;
	font-style:bold;
  position: absolute;
  text-transform: uppercase;
}
</style>
</head>

<body>

  <h2>WDV495 Emerging Tech </h2>

<?php

if ($validForm)

{
?>
	<h3>Thank You!</h3>
    <p>Your submission was successful!</p>

<?php
}
else
{
?>

    <div id="orderArea">
    <form id="form1" name="form1" method="post" action="quantityForm.php">
      <h3>Quantity Form</h3>
      <table width="150" border="0">
        <tr>
          <td>Quantity:</td>
          <td><input type="text" name="inQuantity" id="inQuantity" size="5" value="<?php echo $inQuantity; //place data back in field ?>"/></td>
          <td class="error"><?php echo "$quanErrMsg"; //place error message on form  ?></td>
        </tr>
      </table>
      <p>
        <input type="submit" name="submit" id="submit" value="Submit" />
        <input type="reset" name="reset" id="reset" value="Reset" />
      </p>
    </form>
    </div>

<?php
}
?>
</body>
</html>
