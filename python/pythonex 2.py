#!/usr/local/python-2.7/bin/python2

import cgi
import time

form = cgi.FieldStorage()
name = form.getvalue('name')




print "Content-Type: text/html"
print 

print """
<!DOCTYPE html>
	<html>
		<head>
		
		<title>Learning Python:</title>
		<style> </style>
		
		</head>
		
		<body>
			<h1>Python App</h1> """
print """<h2> My Grocery List: (using python dictionaries)</h2>"""

prices = {'banana': 0.40, 'orange': 0.60, 'apple': 0.75}
my_purchase = {'apple': 2, 'banana': 6, 'orange': 20}
grocery_bill = sum(prices[fruit] * my_purchase[fruit] for fruit in my_purchase)

print """<p>"""
print "I bought", my_purchase['apple'], "apples at $",  prices['apple'], "per apple." 
print """</p>"""


print """<p>"""
print "I bought", my_purchase['orange'], "oranges at $", prices['orange'], "per orange."
print """</p>"""


print """<p>"""
print "I bought", my_purchase['banana'], "pounds of bananas at $", prices['banana'], "per pound."
print """</p>"""


print """<p> <strong>"""
print "I owe the grocer $%.2f" % grocery_bill
print """</strong></p>"""

print """<h2> Magic Eight Ball</h2>"""
print """<iframe src="https://trinket.io/embed/python/1bfc8e5ad7" width="100%" height="600" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe>"""
print		"""</body>
	</html>
"""
